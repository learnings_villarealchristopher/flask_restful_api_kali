from flask import Flask, request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
from flask_sqlalchemy import get_debug_queries 
from werkzeug.security import generate_password_hash, check_password_hash
from functools import wraps
import uuid
import jwt
import os
import datetime

# Init app
app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))
db_name = 'db2.sqlite'

# Database
app.config['SECRTE_KEY'] = 'mysecretkey'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, db_name)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Init db
db = SQLAlchemy(app)
    
#=====================================================#
#======================= USERS =======================#
#=====================================================#

# User Class
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(50), unique=True)
    username = db.Column(db.String(30), unique=True)
    password = db.Column(db.String(80))
    admin = db.Column(db.Boolean)

def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        if 'mytoken' in request.headers:
            token = request.headers['mytoken']
        if not token:
            return jsonify({'message':'Token is missing!'}), 401
        try:
            data = jwt.decode(token, str(app.config['SECRET_KEY']))
            current_user = User.query.filter_by(public_id=data['public_id']).first()
        except:
            return jsonify({'message': 'Token is invalid!'}), 401
        return f(current_user, *args, **kwargs)
    return decorated

# Create/Add user
@app.route('/create_user', methods=['POST'])
def create_user():
    data = request.get_json()

    # check if the username already exists
    user = User.query.filter_by(username=data['username']).first()
    if user:
        return jsonify({'message': 'Failed: Username already exists.'})
    hashed_password = generate_password_hash(data['password'], method='sha256')
    new_user = User(public_id=str(uuid.uuid4()), username=data['username'], password=hashed_password, admin=bool(data['admin']))
    db.session.add(new_user)
    db.session.commit()
    return jsonify({'message': f"New user ({data['username']}) has been successfully created!"})

# Login and get token authentication
@app.route('/')
@app.route('/login', methods=['GET'])
def login():
    auth = request.authorization
    if not auth or not auth.username or not auth.password:
        return make_response("Could not verify. Please enter valid credentials.", 401, 
                {'WWW-Authenticate':'Basic realm="Login required!"'})
    user = User.query.filter_by(username=auth.username).first()
    if check_password_hash(user.password, auth.password):
        token = jwt.encode({
            'public_id': user.public_id, 
            'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=240)}, str(app.config['SECRET_KEY']))
        return jsonify({'token': token.decode('UTF-8')})
    return make_response("Could not verify. Please enter valid credentials.", 401, 
            {'WWW-Authenticate':'Basic realm="Login required!"'})

# Get all users
@app.route('/users', methods=['GET'])
@token_required
def get_all_users(current_user):
    if not current_user.admin:
        return jsonify({'message': 'Unable to view, need admin access.'})
    users = User.query.all()
    output = []
    for user in users:
        user_data = dict()
        user_data['id'] = user.id
        user_data['publc_id'] = user.public_id
        user_data['username'] = user.username
        user_data['password'] = user.password
        user_data['admin'] = user.admin
        output.append(user_data)
    return jsonify({'users': output})

# Get one user
@app.route('/user/<public_id>', methods=['GET'])
@token_required
def get_one_user(current_user, public_id):
    if not current_user.admin:
        return jsonify({'message': 'Unable to view, need admin access.'})
    user = User.query.filter_by(public_id=public_id).first()
    if not user:
        return jsonify({"message": "User not found!"})
    user_data = {}
    user_data['publc_id'] = user.public_id
    user_data['username'] = user.username
    user_data['password'] = user.password
    user_data['admin'] = user.admin
    return jsonify({'users': user_data})

# Update user
@app.route('/user/<public_id>', methods=['PUT'])
@token_required
def update_user(current_user, public_id):
    if not current_user.admin:
        return jsonify({'message': 'Unable to update, need admin access.'})
     
    # Note that we only update password and admin status, not the username.
    user = User.query.filter_by(public_id=public_id).first()
    user.password = generate_password_hash(request.json['password'], method='sha256')
    user.admin = request.json['admin']
    db.session.commit()
    return jsonify({'message': f"User '{user.username}' with public id '#{public_id}' successfully updated!"})

# Delete user 
@app.route('/user/<public_id>', methods=['DELETE'])
@token_required
def delete_user(current_user, public_id):
    if not current_user.admin:
        return jsonify({'message': 'Unable to delete, need admin access.'})
    user = User.query.filter_by(public_id=public_id).first()  
    if not user:
    	return jsonify({'message': 'Unable to delete. User not found!'})
    db.session.delete(user)
    db.session.commit()
    return jsonify({'message': f'User with public id#{public_id} has been deleted!'})
 
# Run server
if __name__ == '__main__':
    app.run(debug=True)


