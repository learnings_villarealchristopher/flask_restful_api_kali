#!/usr/bin/python3

import json

dict_sample = {
    'name': 'Christopher Villareal',
    'age': 23,
    'gender': 'male',
    'address': '1234 kanto.st Brgy. Tanod, Metropolitan City'
        }

print("Python Dictionary Data")
print(dict_sample)


print("\nJSON data")
print(json.dumps(dict_sample))


